
var jsondata;
var actions_data;
var charts;
var neigh_average = 1;	
var avg;
var user = $('#user_oid').val();
var monthly_avg, daily_avg, weekly_avg;
var curr_range = 'Day';
var curr_url = "http://146.185.147.109:8080/consumption/ServiceViewSmartH2O/ConsumptionData/getConsumption?user_id="+user;
$('#mydiv').show();

$.ajax({ 
 	type: 'get',
    	//url: "http://127.0.0.1:8080/SmartH20RestServices/RESTServices/GetConsumptionData/getConsumpData.do?user_oid="+user, 
		url: "http://localhost:8080/community/RestCall.jsp?curr_url="+curr_url, 
		dataType: 'json',
    	success: function(xhr,status){   
			jsondata = xhr;
			$.ajax({ 
				type: 'get',
					//url: "http://localhost:8080/community/getActionsByUser.jsp", 
					url: "http://127.0.0.1:8080/community/UserActivityCreditWebServiceREST/GetUserActions/getActions.do?user_id="+user, 
					dataType: 'json',
					success: function(xhr,status){   
						//actions_data = xhr.data;
						actions_data = xhr;
						$('#mydiv').hide();
						$('#cons_chart_div').show();
						getNeighbordhoodaAverages();
					},
					error: function(xhr,status){
						$('#mydiv').hide();
						alert("Problem plotting consumption data");
					}
			   });
			
    	},
    	error: function(xhr,status){

    	}
   });
   
	function getNeighbordhoodaAverages(){
		var second_url = "http://89.121.250.90:8082/SmartH2O/ServiceViewSmartH2O/GetNeighbourhoodConsumption/getNeighbourhoodConsumption?user_id="+user;
		$.ajax({ 
		type: 'get',
			//url: "http://127.0.0.1:8080/SmartH20RestServices/RESTServices/GetConsumptionData/getConsumpData.do?user_oid="+user, 
			url: "http://localhost:8080/community/RestCall.jsp?curr_url="+second_url, 
			dataType: 'json',
			success: function(xhr,status){ 

				monthly_avg = xhr[0].month_avg;
				daily_avg = xhr[0].day_avg;
				weekly_avg = xhr[0].week_avg;
				onSuccess();
			},
			error: function(xhr,status){
				monthly_avg = 0;
				daily_avg = 0;
				weekly_avg = 0;
			}
	   });
	}

   	function onSuccess(){
  

	var json = jsondata.reverse();

	//var json = jsondata;
	var dataSerie0 = [];
	var dataSerie1 = [];

	var init_sum = 0;
	var init_count = 0;
	var flags_data = [];
	var flag_temp;
	
		for(var i=0;i<json.length;i++){
	//for(var i=json.length-1;i>=0;i--){
		var obj = json[i];
		for(var key in obj){
			if(key=='timestamp'){
			   t = obj[key];
			   t = t.substring(0, 10);
			}
			else{
			   q = obj[key];
			   
			}
		}
		
		if(q<100){
		   init_sum = init_sum + q;
		   init_count = init_count + 1;
	    }
	    else{
			flag_temp = {};
			flag_temp.x = Date.parse(t);
			flag_temp.title = '!';
			flag_temp.text = 'Critical value: ' + q;
			flags_data.push(flag_temp);
			q = 0;
	    }
		
		dataSerie0.push([Date.parse(t),q]);
	}
	
	
	for(var i=0;i<actions_data.length;i++){
		var obj = actions_data[i];
		for(var key in obj){
			if(key=='timestamp'){
			   t = obj[key];

			}
		}
		dataSerie1.push([Date.parse(t),1]);
	}
	
	

	//avg = Math.floor(init_sum/init_count);
	avg = init_sum/init_count;

	var categoryImgs = ["GamificationCustom/images/clock.png","GamificationCustom/images/man.png","GamificationCustom/images/woman.png","GamificationCustom/images/son.png"];

         $(function() {
			charts = new Highcharts.StockChart(
				{
					chart: {
						type: 'line',
						renderTo: 'container',
						zoomType: 'x'
					},
					title: {
						text: ''
					},
					rangeSelector: {
						enabled : true,
						selected : 0,
						buttons: [{
							type: 'month',
							count: 1,
							text: '1m'
						}, {
							type: 'month',
							count: 3,
							text: '3m'
						}, {
							type: 'month',
							count: 6,
							text: '6m'
						}, {
							type: 'all',
							text: 'All'
						}]
					},
					xAxis: {
						events: {
							setExtremes: function (e) {
								setExtremesHandler(e,updateLines);
							}
						}
					},
					scrollbar: {
                        enabled: false,
                        liveRedraw: false
                    },
					exporting: {
						enabled: false
					},
					tooltip: {
						shared: false,
						formatter: function() {
							var text = '';
							if(this.series.name == 'Consumption') {
								text = Highcharts.dateFormat('%A %e-%b-%Y',
                                          new Date(this.x)) +
									   '<br>' + '<span style="color:#23AAE2>\u25CF</span> ' + this.series.name + ': ' + this.y;
							} else if(this.series.name == 'Myself'){
								text = 'Declared Consumption';
							}
							else{
								text = Highcharts.dateFormat('%A %e-%b-%Y',
                                          new Date(this.x)) +
									   '<br>' + '<span style="color:#23AAE2>\u25CF</span> ' + 'Critical value';
							}
							return text;
						}
					},
					yAxis: [
					{
						min: 0,
						height: 150,
						title: {
							text: 'Water Consumption'
						},
						/*
						plotLines : [{
								id: 'NAverage',
								value : 1,
								color : '#23AAE2',
								dashStyle : 'shortdash',
								width : 4,
								zIndex: 2
							}, 
							{
								id: 'Average',
								value : avg,
								color : 'red',
								dashStyle : 'shortdash',
								width : 4,
								zIndex: 3
							}],
						*/
						/*
						plotBands: {
							id: 'BlockRates',
							color: '#acd5b4', // Color value
							from: '0', // Start of the plot band
							to: '0.3', // End of the plot band
							zIndex: 1
						}
						*/
						
					},
					{
						title: {
							text: 'Family Consumption'
						},
						gridLineWidth: 0,
						labels:
						{
						  style: {
										color: 'white',
									}
						},
						min : 0.5,
						max : 2.5,
						top: 150,
						bottom: 200,
						height: 100,
						offset: 0,
						plotBands: [{
							color: '#E5E5E1',
							from: '0.6',
							to: '1.4'
						  }
						  ],
						labels: {
							align: 'left',
							x: 5,
							useHTML: true,  
							formatter: function(){
								if((this.value<2)&&(this.value>0)){
								    var urlmy = $('img[id="myself"]').attr('src');
									//$('img[id="copy"]').attr("src", urlmy);
									//return '<img src="'+categoryImgs[this.value]+'"></img>';
									return '<img height="32" width="32" src="'+urlmy+'"></img>'
								}
								                        
							}
						}
					}],
					navigator: {
						series: {
							data: dataSerie0
						},
						adaptToUpdatedData: false,
					},
					
					series: [{
						name: 'Consumption',
						type: 'column',
						data: dataSerie0,
						//dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['day',[1]]]},
						showInLegend: false,
						color: '#23AAE2',
						zIndex: 4

					},{
						name: 'Myself',
						yAxis: 1,
						data: dataSerie1,
						showInLegend: false,
						marker : {
							enabled : true,
							symbol: 'url(GamificationCustom/images/tap.png)'
						},
						lineWidth : 0,
						enableMouseTracking: true,
						states : {
							hover : false
						}
					},
					{
						type : 'flags',
						data : flags_data,
						onSeries : 'Consumption',
						shape : 'circlepin',
						width : 16,
						color : Highcharts.getOptions().colors[0], // same as onSeries
						fillColor : 'red',
						style : {// text style
							color : 'white'
						},
						states : {
							hover : {
								fillColor : '#395C84' // darker
							}
						}
					}]
					
				},function opt(charts){

					//charts.yAxis[0].plotLinesAndBands[0].label.toFront();
					//charts.yAxis[0].plotLinesAndBands[1].label.toFront();
					//charts.yAxis[0].plotLinesAndBands[2].label.toFront();


			});
    });
		
		function setExtremesHandler(e, callback){
			//console.log(Highcharts.dateFormat(null, e.min));
			//console.log(Highcharts.dateFormat(null, e.max));
			var data = charts.series[2].data;
			var sum = 0;
			var count = 0;
			for(var i=0;i<data.length;i++){
				if(data[i]!=undefined){
					if(data[i].x - e.min >= 0&&e.max - data[i].x >= 0){
						sum = sum + data[i].y;
						count = count + 1;
					}
				}
			}
			if(count!=0){
				//var avg = Math.floor(sum/count);
				var avg = sum/count;
				charts.yAxis[0].removePlotLine('Average');
				callback(avg);

			}
			
			
		}
		
		function updateLines(avg) {
			// Callback, update lines
			if($('#average').prop('checked')){
			charts.yAxis[0].addPlotLine({
                id: 'Average',
				value : avg,
				color : 'red',
				dashStyle : 'shortdash',
				width : 4,
				zIndex: 3
            });
			}
		}


	
	}
		function dayRange() {
			curr_range = "Day";
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['day',[1]]]}});
			charts.series[1].update({ dataGrouping: { approximation: "average",	enabled: true, forced: true, units: [['day',[1]]]}});

		}
		function weekRange() {
			curr_range = "Week";
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['week',[1]]]}});
			charts.series[1].update({ dataGrouping: { approximation: "average",	enabled: true, forced: true, units: [['week',[1]]]}});

		}
		function monthRange(){
			curr_range = "Month";
			charts.series[0].update({ dataGrouping: { approximation: "sum",	enabled: true, forced: true, units: [['month',[1]]] } });
			charts.series[1].update({ dataGrouping: { approximation: "average",	enabled: true, forced: true, units: [['month',[1]]]}});

		}
		
		function showHideLines(checkbox)
		{

			if (checkbox.checked)
			{

				if(checkbox.name=='Average'){
						charts.yAxis[0].addPlotLine({
							id: 'Average',
							value : avg,
							color : 'red',
							dashStyle : 'shortdash',
							width : 4,
							zIndex: 3
					});
				}
				else if(checkbox.name=='NAverage'){
					if(curr_range=="Day"){
						curr_range_avg = daily_avg;
						if(daily_avg > charts.yAxis[0].max){
							charts.yAxis[0].update({ max: daily_avg });
						}
						
					}
					if(curr_range=="Week"){
						curr_range_avg = weekly_avg;
						if(weekly_avg > charts.yAxis[0].max){
							charts.yAxis[0].update({ max: weekly_avg });
						}
					}
					if(curr_range=="Month"){
						curr_range_avg = monthly_avg;
						if(monthly_avg > charts.yAxis[0].max){
							charts.yAxis[0].update({ max: monthly_avg });
						}
					}
					
					charts.yAxis[0].addPlotLine({
						id: 'NAverage',
						value : curr_range_avg,
						color : '#23AAE2',
						dashStyle : 'shortdash',
						width : 4,
						zIndex: 2
					});
				}
						
			}
			else{
				charts.yAxis[0].update({ max: null });
				charts.yAxis[0].removePlotLine(checkbox.name);
			}
		}
		
		function showHideBand(checkbox)
		{
			if (checkbox.checked){
				charts.yAxis[0].update({
				plotBands: {
						id: 'BlockRates',
						color: '#acd5b4', 
						from: '0', 
						to: '0.3', 
						zIndex: 1
					}
				});
			}
			else{
				charts.yAxis[0].removePlotBand('BlockRates');
			}
		}