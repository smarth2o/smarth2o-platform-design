
var badgesdata;
var user_oid = $('#user_oid').val();
var curr_lang = $('#curr_lang').val();
var th_area = $('#thematic_area').val();
var score;
var areaTitles = [];
var areaIds = [];
var areaImages = ['GamificationCustom/images/social.png','GamificationCustom/images/info.png','GamificationCustom/images/drop_area.png','GamificationCustom/images/book.png'];
var areaColors = ['#C2216E','#D28C22','#26A5DD','#74AB44'];
var cardinalities = [];
var max;
var h_new;
var i_new;
var source;
switch(th_area) {
	case "Saving Water":
		source = 2;
		break;
	case "Water Saving Insights":
		source = 3;
		break;
	case "Profiling":
		source = 1;
		break;
	case "Participation":
		source = 0;
		break;
	
}
var localized_string;

if(curr_lang == 'en'){
	localized_string = 'Your '+th_area+' Badges';
}
else{
	localized_string = 'I tuoi badge per '+th_area;
}
	$.ajax({ 
		type: 'get',
			url: "http://127.0.0.1:8080/community/UserActivityCreditWebServiceREST/GetUserBadges/getBadges.do?user_id="+user_oid, 
			dataType: 'json',
			success: function(xhr,status){   
				badgesdata = xhr.badges_list;
				score = xhr.data;
				onSuccessBadge();
			},
			error: function(xhr,status){

			}
	   });
	   

function onSuccessBadge() { 

		for(var h=0;h<badgesdata.length;h++){
				if(badgesdata[h].area==th_area){
					areaIds.push(h);
					areaTitles.push(badgesdata[h].area);			
					cardinalities.push(badgesdata[h].badges.length);
					
				}
				if(h==badgesdata.length-1){
						max = Math.max.apply(null, cardinalities);
				}
				if(score[h].score>badgesdata[h].max){
					badgesdata[h].max = score[h].score;
				}
				
			}
         $(function() {
			single_badge_chart = new Highcharts.Chart(
				{
					chart: {
						renderTo: 'SingleBadgeChartCont'
					},
					title: {
						text: localized_string,
						style: {
									fontSize: '14px',
									fontFamily: 'Helvetica Neue, Helvetica, Arial, sans-serif'
								}
					},
					exporting: {
						enabled: false
					},
					xAxis: {
						//categories: ['Water Saving', 'Learning', 'Profiling', 'Participation'],
						categories: areaIds,
						labels: {
							x: -5,
							useHTML: true,
							formatter: function () {
								
								return '<img height="32" width="32" src="'+areaImages[source]+'" title="'+areaTitles[0]+'"></img>'
							}
							
						},
						gridLineWidth: 0
					},
					yAxis: {
						min: 0,
						max: 110,
						title: {
							text: ''
						},
						labels:
						{
						  enabled: false
						},
						gridLineWidth: 0
					},
					legend: {
						enabled: false
					},
					tooltip: {
						formatter: function() {
							if(this.series.name=='Current credits'){
								return '<b>'+ this.series.name +'</b>: '+ score[this.x].score;
							}
							if(this.series.name=='Badge'){
								return '<b>'+ this.series.name +'</b>: '+ Math.floor(this.y*0.01*badgesdata[this.x].max) + ' credits';
							}
						}
					},

					series: [{
						name: 'Remaining credits',
						type: 'bar',
						data: [{y: 100-Math.floor(score[source].score*100/badgesdata[source].max), color: '#aaa'}],
						stacking: 'normal'
					}, {
						name: 'Current credits',
						type: 'bar',
						data: [{y: Math.floor(score[source].score*100/badgesdata[source].max), color: areaColors[source]}],
						stacking: 'normal'
					}
					 ]
					
					
				},function opt(single_badge_chart){

					
					var tempBadge = {};
					var tempMarker = {};
					var data;
					//for(var h=0; h < max; h++){
					for (var h=max-1; h >=0; h--) {
						
						data = [];
						for(var i=0; i < badgesdata.length; i++){
							if(badgesdata[i].area==th_area){
							
								tempBadge = {};
								
								if(h<badgesdata[i].badges.length){

									tempBadge.y = badgesdata[i].badges[h].score*100/badgesdata[i].max;
									tempMarker = {};
									tempMarker.symbol = 'url(data:image/png;base64,'+badgesdata[i].badges[h].icon.replace(/(\r\n|\n|\r)/gm,"")+')';
									tempMarker.height = '32';
									tempMarker.width = '32';
									tempBadge.marker = tempMarker;
									
								}
								else{
									tempBadge = null;
								}
								data.push(tempBadge);
							}
							
						}
						
						single_badge_chart.addSeries({
								name: 'Badge',
								data: data,
								lineWidth : 0,
								enableMouseTracking: true,
								states : {
									hover : false
								}


						});
						$.each( $('#SingleBadgeChartCont').find('.highcharts-markers').children(), function(){
							var marker = $(this);
							// rotate 90 degrees around the middle point of the marker
							var rotateAttr = 'rotate(90,'+(parseFloat(marker.attr('x'))+parseFloat(marker.attr('width'))/2)+','+marker.attr('y')+')';
							marker.attr('transform',rotateAttr);
						});

					}

			});
    });
		
}	


