var billdata;
var user = $('#user_oid').val();
//var curr_url = "http://esb.smarth2o.ro:9081/bill/ServiceViewSmartH2O/BillData/getBill?user_id=2";
var curr_url = "http://89.121.250.90:8082/SmartH2O/ServiceViewSmartH2O/GetThisMonthTotalConsumption/getThisMonthTotalConsumption?user_id="+user;

$.ajax({ 
 	type: 'get',
    	//url: "http://127.0.0.1:8080/SmartH20RestServices/RESTServices/GetLastMonthTotal/getLastMonthTotal.do?user_oid="+user, 
		url: "http://localhost:8080/community/RestCall.jsp?curr_url="+curr_url,
		dataType: 'json',
    	success: function(xhr,status){   
			billdata = xhr[0];
			onSuccessBill();
    	},
    	error: function(xhr,status){

    	}
   });

   function onSuccessBill() { 

		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = mm+'/'+yyyy;
		
	    $("#bill").html(parseFloat(billdata.monthlyTotalConsumption).toFixed(2) + " m<sup>3</sup>");
	    $("#bill_date").html(today);

   };