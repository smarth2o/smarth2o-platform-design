var goaldata;
var goaldata_copy;
var images = [];
var curr_avg;
//var user_oid = $('#user_oid').val();
//var credits = $('#credits').val();

$.ajax({ 
 	type: 'get',
    	//url: "http://localhost:8080/community/goal.jsp?userID="+user_oid, 
		url: "http://127.0.0.1:8080/community/UserActivityCreditWebServiceREST/GetUserConsumptionGoals/getGoals.do?user_id="+2, 
		dataType: 'json',
    	success: function(xhr,status){   

			goaldata = xhr;
			goaldata_copy = xhr;
			secondCall();
			//onSuccessGoal();
    	},
    	error: function(xhr,status){

    	}
   });

function secondCall(){

	$.ajax({ 
		type: 'get',
			url: "http://127.0.0.1:8080/SmartH20RestServices/RESTServices/GetLastMonthAverage/getLastMonthAverage.do?user_oid="+2, 
			dataType: 'json',
			success: function(xhr,status){   
				curr_avg = xhr[0].avg;
				onSuccessGoal();
			},
			error: function(xhr,status){

			}
	   });
	   
} 
 


function onSuccessGoal() { 

	var scaled_avg;
	var goals = [];
	var scaled_goals = [];
	var sg = [];
	var ticks = [];
	var scaled_ticks = [];
	var titles = [];

	/*
	var tempGoal = {};
	tempGoal.goal = curr_avg;
	tempGoal.title = "Current Average";
	goals.push(tempGoal);
	*/
	
	for(var h=0;h<goaldata.length;h++){
		var tempGoal = {};
		tempGoal.goal = goaldata[h].score;
		tempGoal.title = goaldata[h].title;
		tempGoal.image = goaldata[h].image;
		//tempGoal.image = images[h];
		goals.push(tempGoal);
		ticks.push(goaldata[h].score);
	}
	
	goals.sort(function(a,b) { return parseFloat(a.goal) - parseFloat(b.goal) } );
	ticks.sort();
	var max_of_array = Math.max(goals[goals.length-1].goal, curr_avg);
	scaled_avg = Math.floor(curr_avg*100/max_of_array);
	var h;
	
	for(h=0;h<goals.length;h++){
		(function(){
			
			var sc_go = Math.floor(goals[h].goal*100/max_of_array);
			var tempGoalSc = {};
			tempGoalSc.goal = sc_go;
			tempGoalSc.title = goals[h].title;
			tempGoalSc.image = goals[h].image;
			scaled_goals.push(tempGoalSc);
			sg.push(sc_go);
			//scaled_goals.push(sc_go);
		}());
	}
	//min tick
	scaled_ticks.push(0);
	for(h=0;h<ticks.length;h++){
		(function(){
			var sc_ti = Math.floor(ticks[h]*100/max_of_array);
			scaled_ticks.push(sc_ti);
		}());
		
	}
	//max tick
	scaled_ticks.push(118);

	

	var colors = ['GreenYellow','Orange','OrangeRed','Crimson','Gold','Magenta','DarkGreen',];
	var plotLines = [];
	var found = false;
	var ind;
	
	for(var k=0;k<scaled_goals.length;k++){
		
		var temp = {};
		//var color = getRandomColor();
		var color = colors[k];
		temp.value = scaled_goals[k].goal;
		temp.color = color;
		temp.dashStyle = 'shortdash';
		temp.width = 4;
		temp.zIndex = 4;
		var label = {};
		//label.text = scaled_goals[k].title;
		label.text = '';
		label.style = {};
		label.style.color = color;
		label.align = 'center';
		temp.label = label;
	
		plotLines.push(temp);
		
		//check last badge active
		if((scaled_goals[k].goal>scaled_avg)&&(found==false)){
			found = true;
			ind = k;
		}
	}
	var date = new Date();

	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	var miss_days = lastDay.getDate() - date.getDate();
	if(found){
		$('#goal_title').html('<b>Reduce consumption</b></br><b>'+miss_days+'</b> days left to achieve goals');
	}
	else{
		$('#goal_title').html('<b>Reduce consumption</b></br><b>'+miss_days+'</b> days left to achieve goals');
	}
	
	
	/*
	var avgLine = {};
	var color = "rgba(1, 1, 1,0)";
	avgLine.value = scaled_avg;
	avgLine.color = color;
	avgLine.dashStyle = 'shortdash';
	avgLine.width = 4;
	avgLine.zIndex = 4;
	var label = {};
	label.text = "Current Average";
	label.style = {};
	label.style.color = '#23AAE2';
	label.style.fontWeight = 'bold';
	label.align = 'center';
	avgLine.label = label;
	plotLines.push(avgLine);
	*/
	/*
	function getRandomColor() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
	*/
	
		$(function() {
			goalChart = new Highcharts.Chart(
			{
				chart: {
					type: 'column',
					renderTo: 'goalChartContainer',
					

				},
				title: {
					text: '',
				},
				xAxis: {
					categories: ['Next Goal'],
					labels:
					{
					  enabled: false
					}
				},
				legend: {
					enabled: false
				},
				yAxis: {
					title: null,
					min: 0,
					//max: 110,
					
					labels: {
							align: 'left',
							x: -10,
							useHTML: true,  
							formatter: function(){
								
								if($.inArray(this.value, sg)!=-1){
								//if($.grep(scaled_goals, function(e){ return e.goal == this.value; }).length>0)
									var index = $.inArray(this.value, sg);
									var tit = scaled_goals[index].title;
									if(scaled_goals[index].goal > scaled_avg){
										
										return '<img src="data:image/png;base64, '+scaled_goals[index].image+'" height="32px" width="32px" title="'+tit+'" ></img>';
										//return '<img src="'+scaled_goals[index].image+'" height="32px" width="32px" title="'+tit+'" ></img>';
									}		
									else{
										
										return '<img src="data:image/png;base64, '+scaled_goals[index].image+'" height="32px" width="32px" title="'+tit+'" ></img>';
										//return '<img src="'+scaled_goals[index].image+'" height="32px" width="32px" title="'+tit+'" ></img>';
									}
								}								
							}
						},
						
					//tickInterval: 1,
					//gridLineColor: 'transparent',
					plotLines: plotLines,
					tickPositions: scaled_ticks,

				},
				
				exporting: {
					enabled: false
				},
				series: [{
					name: 'Collected credits',
					data: [scaled_avg],
					marker: {
						enabled: false
					},
					pointWidth: 28,
					color: '#23AAE2',
					zIndex: 1
				}]
					
			},function(goalChart){
				goalChart.renderer.image("GamificationCustom/images/tube.png", 0, 0, goalChart.chartWidth, goalChart.chartHeight).attr({
				  zIndex: 3
				}).add();
				});
				goalChart.series[0].update({ data: [scaled_avg], marker: {enabled: false}, pointWidth: goalChart.chartWidth});


		});
		
		

	var my = 100;
	var similar_households = 80;
	var neighborhood = 110;
	var town = 150;
	var household = 120;
	
	var plotLines = [];
	var array_cons = [];


	//similar households
	temp = {};
	temp.name = "similar households";
	temp.value = similar_households;
	array_cons.push(temp);
	// neighborhood
	var temp = {};
	temp.name = "your neighbors";
	temp.value = neighborhood;
	array_cons.push(temp);
	// town
	var temp = {};
	temp.name = "your town";
	temp.value = town;
	array_cons.push(temp);
	// household
	var temp = {};
	temp.name = "your household";
	temp.value = household;
	array_cons.push(temp);
	
	var max = Math.max.apply(null, array_cons.map(function(a){return a.value;}));

	
	var plotLines = [];
	
	for(var k=0;k<array_cons.length;k++){
		var temp = {};
	
		var color = (array_cons[k].value < my) ? "red":"green";
		temp.value = array_cons[k].value;
		temp.color = color;
		temp.dashStyle = 'shortdash';
		temp.width = 2;
		temp.zIndex = 4;
		var label = {};
		label.text = array_cons[k].name;
		label.style = {};
		label.style.color = (array_cons[k].value < my) ? "red":"green";
		label.align = 'right';
		temp.label = label;

		plotLines.push(temp);
	}
  		$(function() {
			saving_chart = new Highcharts.Chart(
			{
				chart: {
					type: 'column',
					renderTo: 'saving_chart',

				},
				title: {
					text: '',
				},
				xAxis: {
					categories: ['Consumption'],
					labels:
					{
					  enabled: false
					}
				},
				legend: {
					enabled: false
				},
				yAxis: {
					title: null,
					min: 0,
					max: max+max*0.1,
					labels: {
							enabled: false
						},

					gridLineColor: 'transparent',

					plotLines : plotLines,
					reversedStacks: false,
					
				},
				plotOptions: {
					series: {
						point: {
							events: {
								click: function() {
									for (var i = 0; i < this.series.data.length; i++) {
										showBar(this.series.name, this.series.data[i].y)
										
									}

								}
							}
						}
					},
					column: {
						stacking: 'normal',
						
					}
				},
				exporting: {
					enabled: false
				},
				series: [{
					name: 'Today Consumption',
					data: [my],
					color: '#23AAE2',
					marker: {
						enabled: false
					},
					pointWidth: 40,
					zIndex: 1
				},
				{
					name: 'Remaining',
					data: [max+max*0.1-my],
					marker: {
						enabled: false
					},
					pointWidth: 40,
					color: 'LightGray',
					zIndex: 1,
					lineWidth : 0,
	
				},
				{
					name: 'Your town',
					data: [town],
					type: 'spline',
					
					marker: {
						symbol: 'circle'
					},
					
					pointWidth: 40,
					color: (town < my) ? "red":"green",
					zIndex: 5,
					lineWidth : 0,
					
	
				},
				{
					name: 'Similar households',
					data: [similar_households],
					type: 'spline',
					
					marker: {
						symbol: 'circle'
					},
					
					pointWidth: 40,
					color: (similar_households < my) ? "red":"green",
					zIndex: 5,
					lineWidth : 0,
	
				},
				{
					name: 'Your household',
					data: [household],
					type: 'spline',
					
					marker: {
						symbol: 'circle'
					},
					
					pointWidth: 40,
					color: (household < my) ? "red":"green",
					zIndex: 5,
					lineWidth : 0,
	
				},
				{
					name: 'Your neighbors',
					data: [neighborhood],
					type: 'spline',
					
					marker: {
						symbol: 'circle'
					},
					
					pointWidth: 50,
					color: (neighborhood < my) ? "red":"green",
					zIndex: 5,
					lineWidth : 0,
	
				}],

				tooltip: {
					formatter: function() {
						if(this.series.name=='Remaining'){
							return false
						}
						else{
							return '<b>'+this.series.name+'</b>: '+ this.y;
						}
					}
				},
					
			},function(saving_chart){

				});
				


		});


		
		function showBar(id, value){
		/*
			charts.yAxis[0].update({
				plotBands: {
							id: id,
							color: 'rgba(209,250,255,0.5)', // Color value
							from: '0', // Start of the plot band
							to: value, // End of the plot band
							zIndex: 1
						}
			});*/
			if(value>=my){
				saving_chart.yAxis[0].update({
					plotBands: {
								id: id,
								color: 'rgba(179,255,187,0.5)', // Color value
								from: my, // Start of the plot band
								to: value, // End of the plot band
								zIndex: 1
							}
				});
			}
			else{
				saving_chart.yAxis[0].update({
					plotBands: {
								id: id,
								color: 'rgba(255,179,179,0.5)', // Color value
								from: value, // Start of the plot band
								to: my, // End of the plot band
								zIndex: 1
							}
				});
			}
		}

}

jQuery(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) { // on tab selection event
		    jQuery( ".contains-chart" ).each(function() { // target each element with the .contains-chart class
		        var chart = jQuery(this).highcharts(); // target the chart itself
		        chart.reflow() // reflow that chart
		    });
		})